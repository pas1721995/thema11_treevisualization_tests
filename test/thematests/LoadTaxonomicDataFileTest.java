/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.vnijenhuis;

import java.io.File;
import java.io.IOException;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Application to check loadTaxonomicDataFile.
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class LoadTaxonomicDataFileTest {
    String path;
    /**
     * Test if the path is valid. Result should be valid.
     */
    @Test
    public void validFilePathTest() {
        System.out.println("Test: checking a valid file path.");
        path = "C:\\Users\\Vikthor\\Desktop\\RPKM.txt";
        File filePath = new File(path);
        LoadTaxonomicDataFile instance = new LoadTaxonomicDataFile();
        boolean expResult = true;
        boolean result = instance.getTaxonomicFilePath(filePath);
        assertEquals(expResult, result);
    }
    
        /**
     * Test if the file ends with .txt .
     */
    @Test(expected = IllegalArgumentException.class)
    public void isATextFileTest() {
        System.out.println("Test: checking a valid file path.");
        path = "C:\\Users\\Vikthor\\Desktop\\RPKM.tt";
        File filePath = new File(path);
        LoadTaxonomicDataFile instance = new LoadTaxonomicDataFile();
        boolean expResult = true;
        boolean result = instance.getTaxonomicFilePath(filePath);
        assertEquals(expResult, result);
    }
    /**
     * Test if the path is valid. Result should be invalid.
     */
    @Test(expected = IllegalArgumentException.class)
    public void invalidFilePathTest() {
        System.out.println("Test: invalid path.");
        path = "C:\\Users\\Vikthor\\Wololo";
        File filePath = new File(path);
        LoadTaxonomicDataFile instance = new LoadTaxonomicDataFile();
        boolean expResult = false;
        boolean result = instance.getTaxonomicFilePath(filePath);
        assertEquals(expResult, result);
    }
    
    /**
     * Creates an array list of the given text file.
     * @throws IOException 
     */
    @Test
    public void createArrayListTest() throws IOException {
        System.out.println("Test: creates an array list.");
        LoadTaxonomicDataFile instance = new LoadTaxonomicDataFile();
        path = "C:\\Users\\Vikthor\\Desktop\\RPKM.txt";
        List<String> dataArray = instance.createArrayList(path);
        dataArray.stream().forEach((dataNode) -> {
            System.out.println(dataNode);
        });
    }
    /**
     * No file entered to create the test.
     * @throws IOException containing the invalid path.
     */
    @Test(expected = IOException.class)
    public void failToCreateArrayListTest() throws IOException {
        System.out.println("Test: wrong file entered.");
        LoadTaxonomicDataFile instance = new LoadTaxonomicDataFile();
        path = "C:\\Users\\Vikthor\\Desktop\\txt";
        List<String> dataArray = instance.createArrayList(path);
        dataArray.stream().forEach((dataNode) -> {
            System.out.println(dataNode);
        });
    }
}
