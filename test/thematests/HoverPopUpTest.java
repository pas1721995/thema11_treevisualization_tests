/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thematests;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class HoverPopUpTest {
    
    public HoverPopUpTest() {
    }
    
    /**
     * Test of HoverPopUp method, of class HoverPopUp.
     */
    @Test
    public void testHoverPopUp() {
        System.out.println("normal use of HoverPopUp");
        boolean hovering = true;
        HoverPopUp popUp = new HoverPopUp();
        String expResult = "information!";
        String result = popUp.HoverPopUp(hovering);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testHoverPopUpException() {
        System.out.println("not hovering on HoverPopUp");
        boolean hovering = false;
        HoverPopUp popUp = new HoverPopUp();
        String expResult = null;
        String result = popUp.HoverPopUp(hovering);
        assertEquals(expResult, result);
    }
}
