/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.vnijenhuis;

import java.util.Arrays;
import java.util.List;
import org.junit.Test;

/**
 *
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class SetNodeClassesTest {
    
    List<String> primerNodes;
    List<String> allNodes;
    public SetNodeClassesTest() {
    }
    
    @Test
    public void setNodeClassesTest() {
        System.out.println("Test: set node colors to red and green.");
        primerNodes = Arrays.asList("Node 1", "Node 3","Node 5","Node 6","Node 8");
        allNodes = Arrays.asList("Node 1", "Node 2","Node 3","Node 4","Node 5",
                "Node 6","Node 7","Node 8","Node 9","Node 10");
        SetNodeClasses instance = new SetNodeClasses();
        List<List<String>> result = instance.setNodeClass(allNodes, primerNodes);
        for (List<String> nodeList: result) {
            if (nodeList.get(1).equals("green")) {
                System.out.println(nodeList.get(0) + " is " + nodeList.get(1));
            } else {
                System.out.println(nodeList.get(0) + " is " + nodeList.get(1));
            }
        }
    }
}
