/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thematests;

import java.io.FileNotFoundException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class LoadTaxDumpTest {
    
    public LoadTaxDumpTest() {
    }
    
    /**
     * Test of checkTaxDumpPath method, of class LoadTaxDump.
     */
    @Test
    public void testCheckTaxDumpPath() throws Exception {
         System.out.println("normal use scenario of checkTaxDumpPath");
        LoadTaxDump taxDumpLoader = new LoadTaxDump();
        //a path that exists.
        String filePath = "/Users/Koeian/Desktop/test.txt";
        boolean expResult = true;
        boolean result = taxDumpLoader.checkTaxDumpPath(filePath);
        assertEquals(expResult, result);
    }
    
    @Test(expected = FileNotFoundException.class)
    public void testCheckRpkmFilePathException() throws FileNotFoundException {
        System.out.println("normal use scenario of checkTaxDumpPath");
        LoadTaxDump taxDumpLoader = new LoadTaxDump();
        //a path that doesn't exist.
        String filePath = "/Users/Koeian/Desktop/Harry.txt";
        boolean result = taxDumpLoader.checkTaxDumpPath(filePath);
    }
}
