/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thematests;

import java.io.FileNotFoundException;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class CreateTreeNodeLayoutTest {
    
    public CreateTreeNodeLayoutTest() {
    }
    

    @Test
    public void testSelectNodeLayout() {
        System.out.println("normal use scenario of selectNodeLayout");
        CreateTreeNodeLayout createTreeNodeLayout = new CreateTreeNodeLayout();
        //the number of the wanted layout 1=standard 2=other 3=weird.
        int selected = 1;
        String expResult = "Im the standard layout";
        String result = createTreeNodeLayout.selectNodeLayout(selected);
        assertEquals(expResult, result);
    }
    
    @Test
    public void testSelectNodeException() {
        System.out.println("when no number or a number thats not in the options"
                + "is given to selectNodeLayout");
        CreateTreeNodeLayout createTreeNodeLayout = new CreateTreeNodeLayout();
        //the number of the wanted layout 1=standard 2=other 3=weird.
        int selected = 6;
        String expResult = null;
        String result = createTreeNodeLayout.selectNodeLayout(selected);
        assertEquals(expResult, result);
    }
    
}
