/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thematests;

import java.io.File;
import java.io.FileNotFoundException;

/**
 *
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class LoadTaxDump {
    public static void main(String[] args) {
        
    }
    /**
     * Checks if the path for the dumpFile exists and if not it will throw 
     * an error.
     * @param taxDumpPath
     * the path to the dump file
     * @return
     * a boolean that is true if the path exists.
     * @throws FileNotFoundException 
     */
    public boolean checkTaxDumpPath(String taxDumpPath) throws FileNotFoundException {
        if (new File(taxDumpPath).exists()) {
            boolean pathExists = true;
            return pathExists;
        } else {
            throw new FileNotFoundException("Entered path does not exist");
        }
    }
}
