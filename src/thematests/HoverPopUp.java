/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thematests;

/**
 *
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class HoverPopUp {
    public static void main(String[] args) {
        
    }
    /**
     * contains the info about the selected data.
     * @return 
     * the info
     */
    public String popUpInfo() {
        String info = "information!";
        return info;
    }
    
    /**
     * Shows the information about a node if its hovered on.
     * @param hovering
     * a boolean thats true when the cursor is hovering on a node.
     * @return 
     * the information about the node.
     */
    public String HoverPopUp(boolean hovering) {
        String popup;
        if (hovering == true) {
            popup = popUpInfo();
            
        } else {
            popup = null;
        }
        return popup;
    }
}
