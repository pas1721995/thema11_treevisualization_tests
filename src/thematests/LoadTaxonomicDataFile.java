/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.vnijenhuis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Application to load a dataFile.
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class LoadTaxonomicDataFile {  
    /**
     * Function getTaxonomicFilePath()
     * @param filePath contains the path to a file.
     * @return true if there are no errors.
     */
    public final boolean getTaxonomicFilePath(File filePath) {
        if (!filePath.exists()) {
             System.out.println("Invalid path " + filePath);
             throw new IllegalArgumentException("File path " +  filePath 
                    + " is invalid.");
        } else if (!filePath.toString().endsWith(".txt")){
            throw new IllegalArgumentException("File path " +  filePath 
                    + " is invalid.");
        } else {
            System.out.println("Valid path " + filePath);
        }
    return true;
    }
    
    /**
     * Function createArrayList()
     * @param filePath contains a path to a given file.
     * @return ArrayList with data.
     * @throws IOException if a wrong file has been entered it returns this file or file path.
     */
    public final List<String> createArrayList(String filePath) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            List<String> fileData = new ArrayList<>();
            String line = reader.readLine();
            while (line != null) {
                fileData.add(line);
                line = reader.readLine();
            }
        return fileData;
        } catch (IOException e) {
            throw new IOException("An error ocurred at: " + e);
        }
          
    }

}
