/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thematests;

/**
 *
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class CreateTreeNodeLayout {
    public static void main(String[] args) {
        
    }
    /**
     * a layout for the nodes
     * @return 
     * the layout algorithm
     */
    private String standardLayout() {
       String layout = "Im the standard layout";
       return layout;
    }
    /**
     * a layout for the nodes
     * @return 
     * the layout algorithm
     */
    private String otherLayout() {
       String layout = "Im the other layout";
       return layout;
    }
    
    /**
     * a layout for the nodes
     * @return 
     * the layout algorithm
     */
    private String weirdLayout() {
       String layout = "Im the weird layout";
       return layout;
    }
    /**
     * Selects the Node Layout from a switch list.
     * @param selectedLayout
     * the user entered algorithm
     * @return 
     * the algorithm.
     */
    public String selectNodeLayout(int selectedLayout) {
        String layout = null;
        switch(selectedLayout){
            case 1: layout = standardLayout();
                    break;
            case 2: layout = otherLayout();
                    break;
            case 3: layout = weirdLayout();
                    break;
            default: layout = null;   
        }
        return layout;
    }
     
}
