/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.bioinf.vnijenhuis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author raozinga,vnijenhuis,prfolkersma
 */
public class SetNodeClasses {
    String color;
    List<List<String>> nodeColors = new ArrayList<>();
    List<String> addColor;
    public final List<List<String>> setNodeClass(List<String> allNodes, List<String> primerNodes) {
        for (String node: allNodes) {
            if (primerNodes.contains(node)) {
                color = "green";
                addColor = Arrays.asList(node, color);
                nodeColors.add(addColor);
            } else {
                color = "red";
                addColor = Arrays.asList(node, color);
                nodeColors.add(addColor);
            }
        }
        System.out.println(nodeColors);
        return nodeColors;
    }

}
